# Terraform 



## What is terraform?
Terraform is a part of infrastructure as code, which is when your infrastructure is coded. Infrastructure is code, the rest is abstracted by our clound prividers as the provide IaaS (infrastructure as a service).

In IAC, you have configuration management and orchestration tools.

### Configuration Management
• Configuration management = setting up individual servers to perfect desired state
Or managing their mutations.

- Chef
- Ansible
- Shell
- Puppet

### Orechestration Management tools
These manages the networking and actyal infrastructure(size of machines, subnet, vpc, SG, IGW, routes). And deploy instances (machines) into these from AMI's. They can also run small init scripts (UserData, the little thing in AWS when launch instances)
- Terraform
- Cloudformation (aws terraform)


## Terraform Objectives?
objective is to launch and build VPCs and AMIs, including running init scrupts and starting services

## Example usuage
- Terraform creates VPCs
- Terraform setups SGs and NCLs and subnets
- Terraform deploys AMIs into respective subnets and associates SG to instances
- Terraform runs some shell to start services

## Example usage in devOps
- Origin code exists in bitbucket/github
- Automation get triggered from change in source
- CI pulls code into jenkins and runs tests
- Passing tests trigger next job of pipeline (CD)
- Jenkins uses Ansible to change machine to have new code
- Jenkins uses Packer to deliver an AMI 
- Jenkins uses Terraform to deploy AMI into production (CD)
  
## Main commands

```
terraform init
```

## Main Terminology
- Variables
- Resources
- Builders
- Providers

## Install Terraform
## Give permissions

If running from your machine (this not inside AWS) - we need to give it some AWS secrete keys and access keys.

Most clients will manage their keys different depending on their level of sophistication.

### DO NOT 😱
- Hard code your keys with your code.
- Have them on a side file that is ignored by git. Still really poor
  
### Possible way of key management
- set them as enviroment variables for machine and interpolate (low level of sophistication)
- AIM roles
- Encrypted vaults locally
- Cloud vaults

You want the things you want to be encrypted. 

## Example and code
Example EC2 deployment
```terraform



```

## Resource an instance

Syntax

``` terraform

resource "specific_resource" "name_in_tf" {
    param_resource = "specific-value"
    other_param = "value"
}
```

`specific_resource` is a resource within the library of providers
`name_in_tf` is for calling this resource in tf
`param_resource` are specific parameters that can be defines for that resource.
For example a ec2 might require an AMI id where a vps might need an IP range. you need to check your docs.