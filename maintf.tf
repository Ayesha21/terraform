provider "aws" {
    region = "eu-west-1"
    #  access_key = "my-access-key" This is where you could interpolate from ENV 
    #  secret_key = "my-secret-key" This is where you could interpolate from ENV 
}


# variables 
variable "identity-cohort-name" {
    description = "this is the cohort and name of the person"
    default = "cohort7-ayesha"
}

variable "vpc_id" {
    default = "vpc-4bb64132"
}



resource "aws_instance" "web" {
  ami = "ami-0635ca8333e27394d"
  instance_type = "t2.micro"
  subnet_id = "subnet-953a58cf"
  associate_public_ip_address = true
  security_groups = [ "sg-01f897dc42d10c7de", "sg-091c83d3ecee7301e", aws_security_group.sg-webapp.id ]
  tags = {
    "Name" = "${var.identity-cohort-name}-web-tf"
  }
}

# Resource aws_security_group

resource "aws_security_group" "sg-webapp" {
    description = "allows port 80 and 443 into instance and por 22 from our ip"
    vpc_id = var.vpc_id
    ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = [ "0.0.0.0/0" ]
      description = "allows port 80"
    }
    
    ingress {
      from_port = 443
      to_port = 443
      protocol = "tcp"
      cidr_blocks = [ "0.0.0.0/0" ]
      description = "allows port 443"
    }
    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [ "148.252.129.177/32" ]
      description = "allows port 22 from our ip"
    }

    egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    "Name" = "SG-${var.identity-cohort-name}-tf-allow-web-access"
  }
  
}